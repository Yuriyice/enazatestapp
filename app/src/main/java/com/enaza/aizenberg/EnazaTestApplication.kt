package com.enaza.aizenberg

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.enaza.aizenberg.di.interactor.InteractorKoinModule
import com.enaza.aizenberg.di.network.ApiKoinModule
import com.enaza.aizenberg.di.network.CommonNetworkKoinModule
import com.enaza.aizenberg.di.network.NetworkCoinModule
import com.enaza.aizenberg.di.network.UrlKoinModule
import com.enaza.aizenberg.di.repository.RepositoryKoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

//Use multidex app for support old OS versions
class EnazaTestApplication : MultiDexApplication() {

    companion object {
        private lateinit var instance : EnazaTestApplication

        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initializeKoin()
        Timber.plant(Timber.DebugTree())
    }

    private fun initializeKoin() {
        startKoin {
            androidContext(this@EnazaTestApplication)
            modules(arrayListOf(
                ApiKoinModule.getModule(),
                CommonNetworkKoinModule.getModule(),
                NetworkCoinModule.getModule(),
                UrlKoinModule.getModule(),
                RepositoryKoinModule.getModule(),
                InteractorKoinModule.getModule()
            ))
        }
    }



}