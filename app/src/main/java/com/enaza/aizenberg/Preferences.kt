package com.enaza.aizenberg

import android.content.Context
import timber.log.Timber
import java.util.*

class Preferences private constructor() {

    private val sharedPreferences =
        EnazaTestApplication.getInstance().getSharedPreferences("spenaza", Context.MODE_PRIVATE)


    companion object {

        private var instance: Preferences? = null

        fun getInstance(): Preferences = instance ?: synchronized(this) {
            instance ?: Preferences().also {
                instance = it
            }
        }


        private const val KEY_EXPIRES_IN = "kexpin"
        private const val KEY_USER_ID = "keyuserid"
        private const val KEY_ACCESS_TOKEN = "keyactkn"

    }

    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    var userId: Long
        set(value) {
            sharedPreferences.edit().putLong(KEY_USER_ID, value).apply()
        }
        get() {
            return sharedPreferences.getLong(KEY_USER_ID, -1L)
        }

    var expireIn: Long
        set(value) {
            sharedPreferences.edit().putLong(KEY_EXPIRES_IN, value).apply()
        }
        get() {
            return sharedPreferences.getLong(KEY_EXPIRES_IN, -1L)
        }

    var token: String?
        set(value) {
            sharedPreferences.edit().putString(KEY_ACCESS_TOKEN, value).apply()
        }
        get() {
            return sharedPreferences.getString(KEY_ACCESS_TOKEN, null)
        }



    fun isAuthorized() : Boolean {
        if (expireIn == -1L || System.currentTimeMillis() > Date(expireIn).time) {
            sharedPreferences.edit().clear().apply()
            return false
        }
        val b = userId != -1L && token != null
        if (b) {
            Timber.d("$token\n$userId")
        }
        return b
    }
}