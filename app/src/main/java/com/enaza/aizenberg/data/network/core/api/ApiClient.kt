package com.enaza.aizenberg.data.network.core.api

import com.enaza.aizenberg.Preferences
import com.enaza.aizenberg.domain.entity.base.CommonResponse
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.entity.upload.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*


/**
 * Created by Yuriy Aizenberg
 */
interface ApiClient {

    @GET("/method/users.get")
    fun getMyProfile(
        @Query("access_token") token: String = Preferences.getInstance().token!!,
        @Query("v") version: String = "5.103",
        @Query("user_ids") userId: String = Preferences.getInstance().userId.toString(),
        @Query("fields") fields: String = "photo_400_orig"
    ): Single<CommonResponse<List<VKProfileResponse>>>


    @GET("/method/docs.getUploadServer")
    fun getServerLinkForUploading(
        @Query("access_token") token: String = Preferences.getInstance().token!!, @Query("v") version: String = "5.103"
    ): Single<CommonResponse<UploadServerResponse>>


    @Multipart
    @POST
    fun uploadToDocs(
        @Url fullUrl: String, @Part body: MultipartBody.Part, @Query("access_token") token: String = Preferences.getInstance().token!!, @Query(
            "v"
        ) version: String = "5.103"
    ): Single<UploadDocResult>

    @Multipart
    @POST
    fun uploadToAlbum(
        @Url fullUrl: String, @Part body: MultipartBody.Part, @Query("access_token") token: String = Preferences.getInstance().token!!, @Query(
            "v"
        ) version: String = "5.103"
    ): Single<UploadImageResult>

    @POST("/method/docs.save")
    fun commitSaveDoc(
        @Query("file") fileNameFromServer: String, @Query("access_token") token: String = Preferences.getInstance().token!!, @Query(
            "v"
        ) version: String = "5.103"
    ): Single<CommonResponse<VKSavedDocumentResponse>>

    @POST("/method/photos.save")
    fun commitSavePhoto(
        @Query("photos_list") fileNameFromServer: String,
        @Query("album_id") albumId: Long,
        @Query("server") server: Long,
        @Query("hash") hash: String,
        @Query("access_token") token: String = Preferences.getInstance().token!!,
        @Query("v") version: String = "5.103"
    ): Single<CommonResponse<List<VKSavedPhotoResponse>>>


    @GET("/method/photos.getUploadServer")
    fun getServerLinkForUploadingInAlbum(
        @Query("access_token") token: String = Preferences.getInstance().token!!, @Query("v") version: String = "5.103",
        @Query("album_id") albumId: Long
    ): Single<CommonResponse<UploadServerResponse>>


    @POST("/method/wall.post")
    fun postFileToWall(
        @Query("lat") lat: Double?,
        @Query("long") lon: Double?,
        @Query("mute_notifications") mute: Int = 1,
        @Query("attachments") attachments: String,
        @Query("access_token") token: String = Preferences.getInstance().token!!,
        @Query("v") version: String = "5.103"
    ): Single<CommonResponse<VKToWallPostedResponse>>


    @GET("/method/photos.getAlbums")
    fun getPhotoAlbums(
        @Query("access_token") token: String = Preferences.getInstance().token!!,
        @Query("v") version: String = "5.103",
        @Query("need_covers") needCovers: Int = 1
    ): Single<CommonResponse<VKPhotoAlbumsResponseWrapper>>
}