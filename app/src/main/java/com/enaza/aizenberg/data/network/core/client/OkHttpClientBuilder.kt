package com.enaza.aizenberg.data.network.core.client

import okhttp3.ConnectionSpec
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by Yuriy Aizenberg
 */
class OkHttpClientBuilder(private val httpLoggingInterceptor: Interceptor,
                          private val errorHandler: Interceptor) {


    fun build(): OkHttpClient = buildClient()


    private fun buildClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        return builder
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(errorHandler)
                .build()
    }


}