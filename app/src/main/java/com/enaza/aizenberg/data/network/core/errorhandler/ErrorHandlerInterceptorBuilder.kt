package com.enaza.aizenberg.data.network.core.errorhandler


import com.google.gson.Gson
import okhttp3.Interceptor

/**
 * Created by Yuriy Aizenberg
 */
class ErrorHandlerInterceptorBuilder(val gson: Gson) {

    fun build(): Interceptor {
        return createErrorInterceptor()
    }


    private fun createErrorInterceptor(): Interceptor {
        return Interceptor {
            val response = it.proceed(it.request())
            if (!response.isSuccessful) {
                throw HttpServerRequestException(response.code())
            } else {
                response
            }
        }
    }
}