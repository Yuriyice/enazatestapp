package com.enaza.aizenberg.data.network.core.errorhandler

class HttpServerRequestException(val code: Int) : RuntimeException("Something goes wrong. Status $code")
