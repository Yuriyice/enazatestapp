package com.enaza.aizenberg.data.network.core.gson

import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Created by Yuriy Aizenberg
 */
class GsonBuilder {

    fun build(): Gson = create()

    private fun create(): Gson {
        return GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                .create()

    }

}