package com.enaza.aizenberg.data.network.core.logging

import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

/**
 * Created by Yuriy Aizenberg
 */
class HttpLoggingBuilder {

    fun build(): Interceptor = logInterceptor()

    private fun logInterceptor(): Interceptor {
        val interceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.d(message)
            }
        })
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

}