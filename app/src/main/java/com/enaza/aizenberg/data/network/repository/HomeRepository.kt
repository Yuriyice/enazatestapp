package com.enaza.aizenberg.data.network.repository

import com.enaza.aizenberg.data.network.core.api.ApiClient
import com.enaza.aizenberg.data.network.core.retrofit.CountingRequestBody
import com.enaza.aizenberg.domain.entity.base.CommonResponse
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.entity.upload.*
import com.enaza.aizenberg.domain.repository.IHomeRepository
import com.enaza.aizenberg.utils.GeoService
import com.enaza.aizenberg.utils.ZipManager
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import io.reactivex.FlowableEmitter



class HomeRepository(val api: ApiClient) : IHomeRepository {
    override fun getProfile(): Single<CommonResponse<List<VKProfileResponse>>> {
        return api.getMyProfile()
    }

    override fun zipFile(source: File, targetFile: File): Single<String> {
        return Single.create<String> {
            try {
                ZipManager().zip(source.absolutePath, targetFile.absolutePath)
                it.onSuccess(targetFile.absolutePath)
            } catch (e: Exception) {
                it.onError(e)
            }
        }
    }

    override fun getServerLinkForUploading(): Single<CommonResponse<UploadServerResponse>> {
        return api.getServerLinkForUploading()
    }

    override fun getServerLinkForUploadingInAlbum(albumId: Long): Single<CommonResponse<UploadServerResponse>> {
        return api.getServerLinkForUploadingInAlbum(albumId = albumId)
    }

    override fun saveToDocs(
        fullUrl: String,
        file: File,
        emitter: FlowableEmitter<Double>
    ): Single<UploadDocResult> {
        return api.uploadToDocs(fullUrl, createMultipartBody(file.absolutePath,"file", "application/zip", emitter))
    }

    override fun saveToPhoto(
        fullUrl: String,
        file: File,
        emitter: FlowableEmitter<Double>
    ): Single<UploadImageResult> {
        return api.uploadToAlbum(fullUrl, createMultipartBody(file.absolutePath, "file1", "image/*", emitter))
    }

    private fun createMultipartBody(
        filePath: String,
        partName: String,
        mimeType: String,
        emitter: FlowableEmitter<Double>
    ): MultipartBody.Part {
        val file = File(filePath)
        return MultipartBody.Part.createFormData(
            partName,
            file.name,
            createCountingRequestBody(file, mimeType, emitter)
        )
    }

    private fun createCountingRequestBody(
        file: File,
        mimeType: String,
        emitter: FlowableEmitter<Double>
    ): RequestBody {
        val requestBody = createRequestBody(file, mimeType)
        return CountingRequestBody(requestBody, object : CountingRequestBody.Listener {
            override fun onRequestProgress(bytesWritten: Long, contentLength: Long) {
                val progress = 1.0 * bytesWritten / contentLength
                emitter.onNext(progress)
            }

        })
    }

    private fun createRequestBody(file: File, mimeType: String): RequestBody {
        return RequestBody.create(MediaType.parse(mimeType), file)
    }

    override fun commitSaveDoc(fileNameFromServer: String): Single<CommonResponse<VKSavedDocumentResponse>> {
        return api.commitSaveDoc(fileNameFromServer)
    }

    override fun commitSavePhoto(
        fileNameFromServer: String,
        albumId: Long,
        server: Long,
        hash: String
    ): Single<CommonResponse<List<VKSavedPhotoResponse>>> {
        return api.commitSavePhoto(fileNameFromServer, albumId, server, hash)
    }

    override fun postFileToWall(
        fileName: String,
        ownerId: String,
        type: String
    ): Single<CommonResponse<VKToWallPostedResponse>> {
        val location = GeoService.location
        return api.postFileToWall(lat = location?.latitude, lon = location?.longitude, attachments = "$type${ownerId}_$fileName")
    }

    override fun getPhotoAlbums(): Single<CommonResponse<VKPhotoAlbumsResponseWrapper>> {
        return api.getPhotoAlbums()
    }
}