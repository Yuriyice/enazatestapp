package com.enaza.aizenberg.di

import com.enaza.aizenberg.domain.interactor.HomeInteractor
import org.koin.core.KoinComponent
import org.koin.core.inject

class KoinInjector private constructor() : KoinComponent {

    val homeInteractor: HomeInteractor by inject()

    companion object {
        val instance = KoinInjector()
    }

}