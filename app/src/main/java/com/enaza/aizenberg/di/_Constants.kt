package com.enaza.aizenberg.di

const val DI_LOG = "log"
const val DI_ERROR = "err"
const val DI_JSON = "gson"
