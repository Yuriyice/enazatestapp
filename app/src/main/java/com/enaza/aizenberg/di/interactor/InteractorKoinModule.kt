package com.enaza.aizenberg.di.interactor

import com.enaza.aizenberg.domain.interactor.HomeInteractor
import org.koin.dsl.module

object InteractorKoinModule {

    fun getModule() = module {
        single<HomeInteractor> { HomeInteractor(get()) }
    }

}