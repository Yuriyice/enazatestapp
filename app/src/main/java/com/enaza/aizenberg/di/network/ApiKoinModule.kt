package com.enaza.aizenberg.di.network

import com.enaza.aizenberg.data.network.core.api.ApiClient
import org.koin.dsl.module
import retrofit2.Retrofit

object ApiKoinModule {

    fun getModule() = module {
        single<ApiClient> { get<Retrofit>().create(ApiClient::class.java) }
    }
}