package com.enaza.aizenberg.di.network

import com.enaza.aizenberg.data.network.core.errorhandler.ErrorHandlerInterceptorBuilder
import com.enaza.aizenberg.data.network.core.gson.GsonBuilder
import com.enaza.aizenberg.data.network.core.gson.GsonFactoryBuilder
import com.enaza.aizenberg.data.network.core.logging.HttpLoggingBuilder
import com.enaza.aizenberg.di.DI_ERROR
import com.enaza.aizenberg.di.DI_JSON
import com.enaza.aizenberg.di.DI_LOG
import com.google.gson.Gson
import okhttp3.Interceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter

object CommonNetworkKoinModule {

    fun getModule() = module {

        single<Interceptor>(named(DI_LOG)) { HttpLoggingBuilder().build() }
        single<Interceptor>(named(DI_ERROR)) { ErrorHandlerInterceptorBuilder(get()).build() }

        single<Gson> { GsonBuilder().build() }

        single<Converter.Factory>(named(DI_JSON)) { GsonFactoryBuilder(get()).build() }


    }

}