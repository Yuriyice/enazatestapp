package com.enaza.aizenberg.di.network

import com.enaza.aizenberg.data.network.core.client.OkHttpClientBuilder
import com.enaza.aizenberg.data.network.core.retrofit.RetrofitBuilder
import com.enaza.aizenberg.di.DI_ERROR
import com.enaza.aizenberg.di.DI_JSON
import com.enaza.aizenberg.di.DI_LOG
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object NetworkCoinModule {

    fun getModule() = module {
        single { OkHttpClientBuilder(get(named(DI_LOG)), get(named(DI_ERROR))).build() }
        single<Retrofit> { RetrofitBuilder(get(named(DI_JSON)), get(), get()).build() }
    }
}