package com.enaza.aizenberg.di.network

import org.koin.core.qualifier.named
import org.koin.dsl.module

object UrlKoinModule {

    fun getModule() = module {
        single<String> { "https://api.vk.com/" }
    }

}