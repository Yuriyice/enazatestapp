package com.enaza.aizenberg.di.repository

import com.enaza.aizenberg.data.network.repository.HomeRepository
import com.enaza.aizenberg.domain.repository.IHomeRepository
import org.koin.dsl.module

object  RepositoryKoinModule {

    fun getModule() = module {
        single<IHomeRepository> { HomeRepository(get()) }
    }

}