package com.enaza.aizenberg.domain.entity.base

class CommonResponse<T> {

    var response: T? = null
    var error: VKRequestError? = null

    fun isSuccess() = error == null

}