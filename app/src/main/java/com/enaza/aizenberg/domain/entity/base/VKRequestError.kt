package com.enaza.aizenberg.domain.entity.base

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class VKRequestError(
    @SerializedName("error_code")
    val errorCode: Int,
    @SerializedName("error_msg")
    val errorMessage: String
) : Serializable