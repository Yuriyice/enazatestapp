package com.enaza.aizenberg.domain.entity.login

import java.io.Serializable

class VKLoginResponse private constructor() {

    data class Success(val expiresIn: Long, val accessToken: String, val userId: Long) : IVKLoginResponse {
        override fun isSuccess() = true
    }

    data class Failure(val error: String, val errorDescription: String) : IVKLoginResponse {
        override fun isSuccess() = false
    }

    interface IVKLoginResponse : Serializable {

        fun isSuccess() : Boolean

    }

    companion object {

        fun success(expiresIn: Long, accessToken: String, userId: Long) : IVKLoginResponse {
            return Success(expiresIn, accessToken, userId)
        }

        fun failure(error: String, errorDescription: String) : IVKLoginResponse {
            return Failure(error, errorDescription)
        }


    }

}