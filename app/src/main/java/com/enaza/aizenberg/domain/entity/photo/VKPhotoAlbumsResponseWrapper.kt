package com.enaza.aizenberg.domain.entity.photo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class VKPhotoAlbumsResponseWrapper(val count: Int, val items: List<VKPhotoAlbumsResponse>) : Serializable {


    data class VKPhotoAlbumsResponse(
        val id: Long,
        val title: String?,
        @SerializedName("thumb_src")
        val thumb: String?
    ): Serializable


}
