package com.enaza.aizenberg.domain.entity.profile

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class VKProfileResponse(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("photo_400_orig")
    val photo: String
) : Serializable