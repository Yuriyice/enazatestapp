package com.enaza.aizenberg.domain.entity.upload

data class UploadDocResult(val file: String)