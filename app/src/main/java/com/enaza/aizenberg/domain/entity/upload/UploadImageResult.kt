package com.enaza.aizenberg.domain.entity.upload

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken


class UploadImageResult(@SerializedName("photos_list") val stringList: String, val server: Long, val hash: String) {

    var list = ArrayList<UploadedPhoto>()

    fun prepare() {
        list = Gson().fromJson(stringList, object : TypeToken<List<UploadedPhoto>>() {}.type)
    }

    data class UploadedPhoto(val photo: String)
}