package com.enaza.aizenberg.domain.entity.upload

import com.google.gson.annotations.SerializedName

data class UploadServerResponse(
    @SerializedName("upload_url")
    val uploadUrl: String
)