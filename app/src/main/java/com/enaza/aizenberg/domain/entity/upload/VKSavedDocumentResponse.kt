package com.enaza.aizenberg.domain.entity.upload

import java.io.Serializable

data class VKSavedDocumentResponse(val doc: InnerResponse) : Serializable {


    data class InnerResponse(val id: Long)

}



