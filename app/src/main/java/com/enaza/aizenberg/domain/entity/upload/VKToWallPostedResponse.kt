package com.enaza.aizenberg.domain.entity.upload

import com.google.gson.annotations.SerializedName

data class VKToWallPostedResponse(
    @SerializedName("post_id")
    val postId: Long
)