package com.enaza.aizenberg.domain.exception

import com.enaza.aizenberg.domain.entity.base.VKRequestError

class VKApiRequestException(val vkRequestError: VKRequestError) : RuntimeException(vkRequestError.errorMessage)