package com.enaza.aizenberg.domain.interactor

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import com.enaza.aizenberg.EnazaTestApplication
import com.enaza.aizenberg.domain.entity.base.CommonResponse
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.entity.upload.VKToWallPostedResponse
import com.enaza.aizenberg.domain.exception.VKApiRequestException
import com.enaza.aizenberg.domain.repository.IHomeRepository
import com.enaza.aizenberg.utils.FileSaver
import com.enaza.aizenberg.utils.withMultithreadProcessor
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import timber.log.Timber
import java.io.File


class HomeInteractor(val repository: IHomeRepository) {

    fun getMyProfile(): Single<VKProfileResponse> {
        return repository.getProfile().map {
            if (it.isSuccess()) {
                it.response!!.first()
            } else {
                throw VKApiRequestException(it.error!!)
            }
        }.withMultithreadProcessor()
    }

    fun getAlbums(): Single<List<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>> {
        return repository.getPhotoAlbums().map {
            if (it.isSuccess()) {
                it.response!!.items
            } else {
                throw VKApiRequestException(it.error!!)
            }
        }.withMultithreadProcessor()
    }


    @SuppressLint("CheckResult")
    fun processFileToDocumentsAndWall(
        sourceMediaFile: File,
        sourceZipFile: File,
        userId: String
    ): Flowable<Double> {

        return Flowable.create<Double>({
            try {
                repository.zipFile(sourceMediaFile, sourceZipFile).blockingGet()
                val serverResponse = repository.getServerLinkForUploading().blockingGet()
                if (!serverResponse.isSuccess()) throw VKApiRequestException(serverResponse.error!!)
                val docResult = repository.saveToDocs(
                    serverResponse.response!!.uploadUrl,
                    sourceZipFile,
                    it
                ).blockingGet()

                val save = repository.commitSaveDoc(docResult.file).blockingGet()
                if (!save.isSuccess()) throw  VKApiRequestException(save.error!!)
                val postWallResponse = postFileToWall(
                    save.response!!.doc.id.toString(),
                    userId
                ).blockingGet()
                if (!postWallResponse.isSuccess()) {
                    throw VKApiRequestException(postWallResponse.error!!)
                }
                sourceMediaFile.delete()
                sourceZipFile.delete()
                it.onComplete()
            } catch (e: Exception) {
                it.onError(e)
            }
        }, BackpressureStrategy.BUFFER)
            .withMultithreadProcessor()

    }

    private fun postFileToWall(
        fileName: String,
        ownerId: String
    ): Single<CommonResponse<VKToWallPostedResponse>> {
        return repository.postFileToWall(fileName, ownerId, "doc")
    }

    fun dropCookies(context: Context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().removeAllCookies(null)
            } else {
                CookieSyncManager.createInstance(context)
                CookieManager.getInstance().removeAllCookie()
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun saveBitmap(bitmap: Bitmap): Single<File> {
        val file = File(
            EnazaTestApplication.getInstance().externalCacheDir,
            "${System.currentTimeMillis()}.png"
        )
        return FileSaver(bitmap, file).save().withMultithreadProcessor()
    }


    @SuppressLint("CheckResult")
    fun processPhotoToAlbumAndWall(
        sourceMediaFile: File,
        albumId: Long,
        userId: String
    ): Flowable<Double> {

        return Flowable.create<Double>({
            try {
                val serverResponse =
                    repository.getServerLinkForUploadingInAlbum(albumId).blockingGet()

                if (!serverResponse.isSuccess()) throw VKApiRequestException(serverResponse.error!!)
                val serverUrl = serverResponse.response!!.uploadUrl

                val uploadResult =
                    repository.saveToPhoto(serverUrl, sourceMediaFile, it).blockingGet()

                uploadResult.prepare()

                val save = repository.commitSavePhoto(
                    uploadResult.stringList,
                    albumId,
                    uploadResult.server,
                    uploadResult.hash
                ).blockingGet()


                if (!save.isSuccess()) throw  VKApiRequestException(save.error!!)

                val postWallResponse = repository.postFileToWall(
                    save.response!!.first().id.toString(),
                    userId,
                    "photo"
                ).blockingGet()
                if (!postWallResponse.isSuccess()) {
                    throw VKApiRequestException(postWallResponse.error!!)
                }
                sourceMediaFile.delete()
                it.onComplete()
            } catch (e: Exception) {
                it.onError(e)
            }
        }, BackpressureStrategy.BUFFER)
            .withMultithreadProcessor()

    }


}