package com.enaza.aizenberg.domain.repository

import com.enaza.aizenberg.domain.entity.base.CommonResponse
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.entity.upload.*
import io.reactivex.FlowableEmitter
import io.reactivex.Single
import java.io.File

interface IHomeRepository {

    fun getProfile(): Single<CommonResponse<List<VKProfileResponse>>>

    fun zipFile(source: File, targetFile: File): Single<String>

    fun getServerLinkForUploading(): Single<CommonResponse<UploadServerResponse>>

    fun getServerLinkForUploadingInAlbum(albumId: Long) : Single<CommonResponse<UploadServerResponse>>

    fun saveToDocs(fullUrl: String, file: File, emitter: FlowableEmitter<Double>): Single<UploadDocResult>

    fun saveToPhoto(fullUrl: String, file: File, emitter: FlowableEmitter<Double>): Single<UploadImageResult>

    fun commitSaveDoc(fileNameFromServer: String) : Single<CommonResponse<VKSavedDocumentResponse>>

    fun commitSavePhoto(fileNameFromServer: String, albumId: Long, server: Long, hash: String) : Single<CommonResponse<List<VKSavedPhotoResponse>>>

    fun postFileToWall(fileName: String, ownerId: String, type: String) : Single<CommonResponse<VKToWallPostedResponse>>

    fun getPhotoAlbums() : Single<CommonResponse<VKPhotoAlbumsResponseWrapper>>
}