package com.enaza.aizenberg.ui

import android.content.Context
import androidx.annotation.DrawableRes
import com.afollestad.materialdialogs.MaterialDialog

object DialogCreator {

    fun createSuccessDialog(context: Context, title: String, text: String,  @DrawableRes drawableRes: Int ?= null): MaterialDialog {
        return MaterialDialog(context).show {
            title(text = title)
            message(text = text)
            if (drawableRes != null) {
                icon(drawableRes)
            }
        }
    }

    fun createLogoutDialog(context: Context, title: String, text: String, onLogoutResult: ((pressedYes: Boolean, materialDialog: MaterialDialog) -> Unit), @DrawableRes drawableRes: Int? = null) : MaterialDialog {
        return MaterialDialog(context).show {
            title(text = title)
            message(text = text)
            if (drawableRes != null) {
                icon(drawableRes)
            }
            positiveButton(text = "Выход") { onLogoutResult.invoke(true, this) }
            negativeButton (text = "Отмена") { onLogoutResult.invoke(false, this) }
        }
    }

}