package com.enaza.aizenberg.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.enaza.aizenberg.R
import com.enaza.aizenberg.ui.adapter.base.BaseRecyclerAdapter
import com.enaza.aizenberg.ui.home.model.ListActionModel

class ActionModelAdapter(context: Context) :
    BaseRecyclerAdapter<ListActionModel, ActionModelAdapter.ActionVHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActionVHolder {
        return ActionVHolder(inflate(R.layout.item_rv_option, parent))
    }

    override fun onBindViewHolder(holder: ActionVHolder, position: Int) {
        val item = getItem(position)
        holder.imgActionIcon.setImageResource(item.actionDrawResId)
        holder.txtActionName.text = item.actionName
        holder.txtActionDescription.text = item.actionDescription
        bindTouchListener(holder.itemView, position, item)
    }


    class ActionVHolder(itemView: View) : BaseRecyclerAdapter.VHolder(itemView) {
        val imgActionIcon = findView<ImageView>(R.id.imgActionIcon)
        val txtActionName = findView<TextView>(R.id.txtActionName)
        val txtActionDescription = findView<TextView>(R.id.txtActionDescription)
    }
}