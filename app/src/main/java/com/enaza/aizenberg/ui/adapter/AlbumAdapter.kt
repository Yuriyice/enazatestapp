package com.enaza.aizenberg.ui.adapter

import android.content.Context
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.ui.adapter.base.BaseRecyclerAdapter
import com.squareup.picasso.Picasso

class AlbumAdapter(context: Context) :
    BaseRecyclerAdapter<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse, AlbumAdapter.AlbumVHolder>(
        context
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumVHolder {
        return AlbumVHolder(inflate(R.layout.view_album_item, parent))
    }

    override fun onBindViewHolder(holder: AlbumVHolder, position: Int) {
        val data = getItem(position)
        holder.txtAlbumName.text = data.title
        if (data.thumb != null) {
            Picasso.get().load(Uri.parse(data.thumb)).into(holder.imgAlbumCover)
        }
        bindTouchListener(holder.itemView, position, data)
    }


    class AlbumVHolder(itemView: View) : BaseRecyclerAdapter.VHolder(itemView) {
        val txtAlbumName = findView<TextView>(R.id.txtAlbumName)
        val imgAlbumCover = findView<ImageView>(R.id.imgAlbumCover)
    }
}