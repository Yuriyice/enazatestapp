package com.enaza.aizenberg.ui.adapter.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Yuriy Aizenberg
 */
abstract class BaseRecyclerAdapter<T, VH : BaseRecyclerAdapter.VHolder>(protected val context: Context) :
    RecyclerView.Adapter<VH>() {

    var recyclerTouchListener: IRecyclerTouchListener<T>? = null
    var data: MutableList<T> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun isEmpty(): Boolean = data.isEmpty()

    fun getItem(position: Int): T = data[position]

    protected fun inflate(@LayoutRes resId: Int, viewGroup: ViewGroup): View {
        return (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
            resId,
            viewGroup,
            false
        )
    }

    protected fun bindTouchListener(view: View, position: Int, data: T) {
        view.setOnClickListener {
            recyclerTouchListener?.onItemClick(data, position)
        }
    }

    protected fun unbindTouchListener(view: View) {
        view.setOnClickListener(null)
    }

    override fun getItemCount(): Int = data.size

    override fun getItemId(position: Int): Long = position.toLong()


    abstract class VHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun <T : View> findView(@IdRes id: Int): T = itemView.findViewById(id)

    }
}