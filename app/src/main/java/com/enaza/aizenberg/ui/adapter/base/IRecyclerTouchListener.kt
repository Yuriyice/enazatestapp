package com.enaza.aizenberg.ui.adapter.base

interface IRecyclerTouchListener<T> {

    fun onItemClick(data: T, position: Int)

}