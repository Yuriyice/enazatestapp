package com.enaza.aizenberg.ui.home

import android.content.Intent
import android.os.Bundle
import com.enaza.aizenberg.EnazaTestApplication
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.exception.VKApiRequestException
import com.enaza.aizenberg.ui.DialogCreator
import com.enaza.aizenberg.ui.start.welcome.WelcomeActivity
import com.enaza.aizenberg.ui.view.CurrentUserView
import com.enaza.aizenberg.utils.GeoService
import com.enaza.aizenberg.utils.showErrorSnack
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber

class HomeActivity : MvpAppCompatActivity(), HomeViewContract, EasyPermissions.PermissionCallbacks {

    lateinit var currentUserView: CurrentUserView

    private val presenter by moxyPresenter { HomePresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        currentUserView = findViewById(R.id.currentUserView)
        presenter.getProfile()
        currentUserView.onLogout = {
            DialogCreator.createLogoutDialog(this, "Выход", "Выйти и сменить пользователя?", {isYes, dialog ->
                run {
                    if (isYes) {
                        presenter.logout(this)
                    }
                    dialog.cancel()
                }
            },  R.drawable.ic_exit_colorized)
        }
        if (!hasGeoPermissions()) {
            EasyPermissions.requestPermissions(this, "Geolocation listener", LOCATION_PERMISSIONS_REQUEST_CODE, *LOCATION_PERMISSIONS)
        } else {
            GeoService.startGeoListening(EnazaTestApplication.getInstance())
        }

    }

    override fun onProfileReady(vkProfile: VKProfileResponse) {
        currentUserView.setProfile(vkProfile)
    }

    override fun onVkError(vkApiRequestException: VKApiRequestException) {
        showErrorSnack(vkApiRequestException.vkRequestError.errorMessage)
    }

    override fun onError(cause: String) {
        showErrorSnack(cause)
    }

    override fun onLogout() {
        startActivity(Intent(this, WelcomeActivity::class.java))
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (hasGeoPermissions()) {
            GeoService.startGeoListening(EnazaTestApplication.getInstance())
        }
    }

    private fun hasGeoPermissions() : Boolean {
        return EasyPermissions.hasPermissions(this, *LOCATION_PERMISSIONS)
    }

    companion object {
        private val LOCATION_PERMISSIONS = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION)
        private const val LOCATION_PERMISSIONS_REQUEST_CODE = 14444
    }

    override fun onDestroy() {
        GeoService.stopGeoListening()
        super.onDestroy()
    }
}