package com.enaza.aizenberg.ui.home

import android.content.Context
import android.graphics.Bitmap
import com.enaza.aizenberg.Preferences
import com.enaza.aizenberg.di.KoinInjector
import com.enaza.aizenberg.domain.exception.VKApiRequestException
import io.reactivex.disposables.CompositeDisposable
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class HomePresenter : MvpPresenter<HomeViewContract>() {

    val interactor = KoinInjector.instance.homeInteractor
    private val compositeDisposable = CompositeDisposable()

    fun getProfile() {
        compositeDisposable.add(
            interactor.getMyProfile().subscribe({
                viewState.onProfileReady(it)
            }, {
                if (it is VKApiRequestException) {
                    viewState.onVkError(it)
                } else {
                    viewState.onError("Something goes wrong")
                }
            })
        )
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
        super.onDestroy()

    }

    fun logout(context: Context) {
        interactor.dropCookies(context)
        Preferences.getInstance().clear()
        viewState.onLogout()
    }




}