package com.enaza.aizenberg.ui.home

import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse
import com.enaza.aizenberg.domain.exception.VKApiRequestException
import com.enaza.aizenberg.ui.BaseMvpView
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface HomeViewContract : BaseMvpView {


    fun onProfileReady(vkProfile: VKProfileResponse)

    fun onVkError(vkApiRequestException: VKApiRequestException)

    fun onLogout()

}