package com.enaza.aizenberg.ui.home.child.actions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.enaza.aizenberg.R
import com.enaza.aizenberg.ui.adapter.ActionModelAdapter
import com.enaza.aizenberg.ui.adapter.base.IRecyclerTouchListener
import com.enaza.aizenberg.ui.home.model.ListActionModel

class ActionListFragment : Fragment() {

    private lateinit var rvActions: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_actionlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvActions = view.findViewById(R.id.rvActions)
        rvActions.setHasFixedSize(true)
        rvActions.layoutManager = LinearLayoutManager(context!!)

        val adapter = ActionModelAdapter(context!!)
        adapter.setHasStableIds(true)
        adapter.data = arrayListOf(
            ListActionModel(
                R.drawable.ic_mic,
                "Запись звука",
                "Нажатие на “запись” открывает экран записи звука с микрофона. На экране отображается кнопка “Записать”, при её нажатии и удержании осуществляется запись звука с микрофона.",
                ListActionModel.ActionType.AUDIO_RECORD
            ),
            ListActionModel(
                R.drawable.ic_photo,
                "Съёмка и загрузка фотографии в альбом",
                "Нажатие на “фото” открывает экран съёмки фото. После съёмки фото требуется загрузить это фото во Вконтакте в мои фотографии и опубликовать сообщение на стене, прикрепив эту фотографию и текущее местоположение пользователя (если доступ к местоположению есть).",
                ListActionModel.ActionType.PHOTO
            )
        )

        rvActions.adapter = adapter

        adapter.recyclerTouchListener = object : IRecyclerTouchListener<ListActionModel> {
            override fun onItemClick(data: ListActionModel, position: Int) {
                when (data.actionType) {
                    ListActionModel.ActionType.AUDIO_RECORD -> {
                        findNavController().navigate(ActionListFragmentDirections.actionActionListFragment2ToAudioRecordFragment())

                    }
                    ListActionModel.ActionType.PHOTO -> {
                        findNavController().navigate(ActionListFragmentDirections.actionActionListFragment2ToPhotoPickFragment())
                    }
                }
            }

        }
    }


}