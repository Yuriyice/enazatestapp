package com.enaza.aizenberg.ui.home.child.photo

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.ui.DialogCreator
import com.enaza.aizenberg.ui.view.UploadingProgressView
import com.enaza.aizenberg.utils.invisible
import com.enaza.aizenberg.utils.visible
import com.squareup.picasso.Picasso
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import pub.devrel.easypermissions.EasyPermissions
import java.io.File


class PhotoPickFragment : MvpAppCompatFragment(), PhotoViewContract,
    EasyPermissions.PermissionCallbacks {

    lateinit var imgPickedImage: ImageView
    lateinit var llPhotoControlContainer: View
    lateinit var txtPickedAlbum: TextView
    lateinit var imgPickAlbum: ImageView
    lateinit var imgPhotoUpload: ImageView
    lateinit var imgPickRepickImage: ImageView
    lateinit var llPickPhoto: View
    lateinit var uploadingProgressView: UploadingProgressView

    private val photoPresenter by moxyPresenter { PhotoPresenter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pick_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgPickedImage = view.findViewById(R.id.imgPickedImage)
        imgPickAlbum = view.findViewById(R.id.imgPickAlbum)
        txtPickedAlbum = view.findViewById(R.id.txtPickedAlbum)
        imgPhotoUpload = view.findViewById(R.id.imgPhotoUpload)
        imgPickRepickImage = view.findViewById(R.id.imgPickRepickImage)
        llPhotoControlContainer = view.findViewById(R.id.llPhotoControlContainer)
        uploadingProgressView = view.findViewById(R.id.uploadingProgressView)
        llPickPhoto = view.findViewById(R.id.llPickPhoto)
        photoPresenter.getAlbums()
        imgPickRepickImage.setOnClickListener {
            if (hasCameraPermission()) {
                openCamera()
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    "Need camera permission to take a photo",
                    CAMERA_PERMISSION_CODE,
                    CAMERA_PERMISSION,
                    STORAGE_PERMISSION
                )
            }
        }
    }

    private fun openCamera() {
        val chooserIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(chooserIntent, CAMERA_PHOTO)
    }

    private fun hasCameraPermission() =
        EasyPermissions.hasPermissions(context!!, CAMERA_PERMISSION, STORAGE_PERMISSION)

    override fun openAlbumPicker(list: List<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>) {
        val directions =
            PhotoPickFragmentDirections.actionPhotoPickFragmentToPickAlbumFragment(
                PickAlbumFragment.ArgWrapper(
                    list
                )
            )
        findNavController().navigate(directions)
    }

    override fun onPhotoReady(file: File) {
        Picasso.get().load(file).into(imgPickedImage)
    }

    override fun onResume() {
        super.onResume()
        AlbumPickerHolder.data?.let {
            photoPresenter.setAlbum(it)
        }
    }

    override fun onAlbumPicked(album: VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse) {
        txtPickedAlbum.text = album.title ?: "No Title Album"
    }


    override fun lockAlbumPick() {
        llPickPhoto.setOnClickListener {}
        imgPickAlbum.setImageResource(R.drawable.ic_pick_album_inactive)
    }

    override fun unlockAlbumPick() {
        llPickPhoto.setOnClickListener { photoPresenter.onPickAlbumClick() }
        imgPickAlbum.setImageResource(R.drawable.ic_pick_album_active)
    }

    override fun onError(cause: String) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (hasCameraPermission()) {
            openCamera()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PHOTO && resultCode == Activity.RESULT_OK) {
            val photo = data?.extras?.get("data") as Bitmap
            photoPresenter.saveFile(photo)
        }
    }


    override fun unlockUploadButton() {
        imgPhotoUpload.setImageResource(R.drawable.ic_upload_active)
        imgPhotoUpload.setOnClickListener {
            photoPresenter.upload()
        }
    }

    override fun lockUploadButton() {
        imgPhotoUpload.setImageResource(R.drawable.ic_upload_inactive)
        imgPhotoUpload.setOnClickListener {}
    }


    override fun onUploadingStarted() {
        uploadingProgressView.reset()
        uploadingProgressView.visible()
        uploadingProgressView.start()
    }

    override fun onUploadingProcess(value: Double) {
        uploadingProgressView.updateProgress(value)
    }

    override fun onUploadingEnd() {
        uploadingProgressView.stop()
        DialogCreator.createSuccessDialog(context!!, "Готово", "Файл успешно загружен", R.drawable.ic_done)

    }

    override fun onUploadingError() {
        uploadingProgressView.stop()
    }

    override fun hideMainPhoto() {
        imgPickedImage.invisible()
    }

    override fun showMainPhoto() {
        imgPickedImage.visible()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun resetState() {
        imgPickedImage.setImageDrawable(null)
        imgPickedImage.visible()
        uploadingProgressView.stop()
        uploadingProgressView.reset()
        uploadingProgressView.invisible()
        txtPickedAlbum.text = "Выберете альбом для загрузки фото"
        lockUploadButton()
    }

    companion object {
        private const val CAMERA_PERMISSION = android.Manifest.permission.CAMERA
        private const val STORAGE_PERMISSION = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        private const val CAMERA_PERMISSION_CODE = 16666
        private const val CAMERA_PHOTO = 16667
    }


}