package com.enaza.aizenberg.ui.home.child.photo

import android.graphics.Bitmap
import com.enaza.aizenberg.Preferences
import com.enaza.aizenberg.di.KoinInjector
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import io.reactivex.disposables.CompositeDisposable
import moxy.InjectViewState
import moxy.MvpPresenter
import timber.log.Timber
import java.io.File

@InjectViewState
class PhotoPresenter : MvpPresenter<PhotoViewContract>() {


    private var albums = ArrayList<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>()
    private val compositeDisposable = CompositeDisposable()
    private val interactor = KoinInjector.instance.homeInteractor

    private var pickedAlbum: VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse? = null
    private var capturedFilePath: String? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.lockUploadButton()
    }


    fun getAlbums() {
        viewState.lockAlbumPick()
        compositeDisposable.add(interactor.getAlbums().subscribe({
            albums.clear()
            albums.addAll(it)
            viewState.unlockAlbumPick()
        }, {
            viewState.onError(it.message!!)
        }))
    }

    fun onPickAlbumClick() {
        viewState.openAlbumPicker(albums)
    }

    fun setAlbum(album: VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse) {
        pickedAlbum = album
        AlbumPickerHolder.data = null
        viewState.onAlbumPicked(pickedAlbum!!)
        if (capturedFilePath != null) {
            viewState.unlockUploadButton()
        }
    }


    fun saveFile(bitmap: Bitmap) {
        compositeDisposable.add(
            interactor.saveBitmap(bitmap).subscribe({
                viewState.onPhotoReady(it)
                capturedFilePath = it.absolutePath
                if (pickedAlbum != null) {
                    viewState.unlockUploadButton()
                }
            }, {
                viewState.onError(it.message!!)
            })
        )
    }

    fun upload() {
        if (pickedAlbum != null && capturedFilePath != null) {
            viewState.onUploadingStarted()
            viewState.hideMainPhoto()
            compositeDisposable.add(
                interactor.processPhotoToAlbumAndWall(
                    File(capturedFilePath!!),
                    pickedAlbum!!.id,
                    Preferences.getInstance().userId.toString()
                ).subscribe({
                    viewState.onUploadingProcess(it)
                }, {
                    viewState.onUploadingError()
                    viewState.resetState()
                }, {
                    viewState.onUploadingEnd()
                    viewState.resetState()
                })
            )
        }
    }


}