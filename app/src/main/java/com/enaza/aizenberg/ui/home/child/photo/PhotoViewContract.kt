package com.enaza.aizenberg.ui.home.child.photo

import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.ui.BaseMvpView
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType
import java.io.File

@StateStrategyType(AddToEndStrategy::class)
interface PhotoViewContract : BaseMvpView {

    fun lockAlbumPick()

    fun unlockAlbumPick()

    @StateStrategyType(SkipStrategy::class)
    fun openAlbumPicker(list: List<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>)

    fun onAlbumPicked(album: VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse)

    fun unlockUploadButton()

    fun lockUploadButton()

    fun onPhotoReady(file: File)

    fun onUploadingStarted()

    fun onUploadingProcess(value: Double)

    fun onUploadingEnd()

    fun onUploadingError()

    fun hideMainPhoto()

    fun showMainPhoto()

    fun resetState()

}