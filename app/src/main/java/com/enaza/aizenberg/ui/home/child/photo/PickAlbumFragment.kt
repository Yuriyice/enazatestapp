package com.enaza.aizenberg.ui.home.child.photo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.photo.VKPhotoAlbumsResponseWrapper
import com.enaza.aizenberg.ui.adapter.AlbumAdapter
import com.enaza.aizenberg.ui.adapter.base.IRecyclerTouchListener
import java.io.Serializable

class PickAlbumFragment : Fragment() {

    private lateinit var rvAlbums: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pick_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvAlbums = view.findViewById(R.id.rvAlbums)
        rvAlbums.setHasFixedSize(true)
        rvAlbums.layoutManager = GridLayoutManager(context, 2)
        val wrapper = arguments!!.getSerializable("list") as ArgWrapper
        setData(wrapper.data)
    }

    fun setData(data: List<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>) {
        val albumAdapter = AlbumAdapter(context!!)
        rvAlbums.adapter = albumAdapter
        albumAdapter.data = ArrayList(data)
        albumAdapter.recyclerTouchListener =
            object : IRecyclerTouchListener<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse> {
                override fun onItemClick(
                    data: VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse,
                    position: Int
                ) {
                    AlbumPickerHolder.data = data
                    findNavController().popBackStack()
                }

            }
    }


    data class ArgWrapper(val data: List<VKPhotoAlbumsResponseWrapper.VKPhotoAlbumsResponse>) :
        Serializable

}