package com.enaza.aizenberg.ui.home.child.record

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import com.enaza.aizenberg.R
import com.enaza.aizenberg.ui.DialogCreator
import com.enaza.aizenberg.ui.view.RecordTouchHolderOverlay
import com.enaza.aizenberg.ui.view.UploadingProgressView
import com.enaza.aizenberg.utils.disable
import com.enaza.aizenberg.utils.enable
import com.enaza.aizenberg.utils.invisible
import com.enaza.aizenberg.utils.visible
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber

class AudioRecordFragment : MvpAppCompatFragment(), EasyPermissions.PermissionCallbacks,
    AudioRecordingViewContract,
    RecordTouchHolderOverlay.IOverlayListener {

    override fun onRecordStart() {
        recordAnimationView.playAnimation()
    }


    override fun onRecordComplete() {
        recordAnimationView.pauseAnimation()
        recordAnimationView.progress = 0f
    }

    override fun onRecordFailure(cause: String) {
        recordAnimationView.pauseAnimation()
        recordAnimationView.progress = 0f
    }


    private lateinit var recordAnimationView: LottieAnimationView
    private lateinit var imgPlayPause: ImageView
    private lateinit var imgUpload: ImageView
    private lateinit var recordOverlay: RecordTouchHolderOverlay
    private lateinit var uploadingProgressView: UploadingProgressView

    private val presenter by moxyPresenter { AudioRecordingPresenter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_audio_record, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recordAnimationView = view.findViewById(R.id.viewAnimVoiceRecord)
        imgPlayPause = view.findViewById(R.id.imgAudioPlay)
        imgUpload = view.findViewById(R.id.imgAudioUpload)
        recordOverlay = view.findViewById(R.id.recordOverlay)
        uploadingProgressView = view.findViewById(R.id.uploadingProgressView)

        recordAnimationView.progress = 0f
        recordOverlay.touchListener = this
        if (!hasPermission()) {
            EasyPermissions.requestPermissions(
                this,
                "We need this permission for audio recording",
                PERMISSION_REQUEST_CODE,
                PERMISSION
            )
        } else {
            presenter.setPermissionGranted()
            recordOverlay.enable()
        }


        imgPlayPause.setOnClickListener {
            presenter.startOrStopPlaying()
        }

        imgUpload.setOnClickListener {
            presenter.uploadFile()
        }


    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.permissionPermanentlyDenied(this, PERMISSION)) {
            Toast.makeText(
                context!!,
                "Невозможно записать аудио без разрешения",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (hasPermission()) {
            presenter.setPermissionGranted()
            recordOverlay.enable()
        }
    }

    private fun hasPermission(): Boolean {
        return EasyPermissions.hasPermissions(context!!, PERMISSION)
    }

    override fun onStartTouch() {
        presenter.startRecording()
    }

    override fun onEndTouch() {
        presenter.stopRecording()
    }

    override fun setPlayButtonToPlayState(isEnabled: Boolean) {
        imgPlayPause.isEnabled = isEnabled
        //TODO this should be place in drawable file selector
        if (isEnabled) {
            imgPlayPause.setImageResource(R.drawable.ic_play_active)
        } else {
            imgPlayPause.setImageResource(R.drawable.ic_play_inactive)
        }
    }

    override fun setPlayButtonToStopState() {
        imgPlayPause.setImageResource(R.drawable.ic_stop_active)
    }

    override fun activeUploadButton() {
        imgUpload.setImageResource(R.drawable.ic_upload_active)
        imgUpload.enable()
    }

    override fun deactivateUploadButton() {
        imgUpload.setImageResource(R.drawable.ic_upload_inactive)
        imgUpload.disable()
    }

    override fun resetState() {
        super.resetState()
        recordAnimationView.pauseAnimation()
        recordAnimationView.progress = 0f
    }

    companion object {
        private const val PERMISSION = android.Manifest.permission.RECORD_AUDIO
        private const val PERMISSION_REQUEST_CODE = 15556
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onUploadingStarted() {
        uploadingProgressView.reset()
        uploadingProgressView.visible()
        uploadingProgressView.start()
    }

    override fun onUploadingInProgress(progress: Double) {
        Timber.d("Uploading $progress")
        uploadingProgressView.updateProgress(progress)
    }

    override fun onUploadingEnd() {
        uploadingProgressView.stop()
        uploadingProgressView.invisible()
        DialogCreator.createSuccessDialog(context!!, "Готово", "Файл успешно загружен", R.drawable.ic_done)
    }

    override fun onUploadingFailure(cause: String) {
        uploadingProgressView.stop()
        uploadingProgressView.invisible()
    }
}