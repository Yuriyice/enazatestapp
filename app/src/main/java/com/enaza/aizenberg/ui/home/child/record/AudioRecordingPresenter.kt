package com.enaza.aizenberg.ui.home.child.record

import android.media.MediaPlayer
import android.media.MediaRecorder
import com.enaza.aizenberg.EnazaTestApplication
import com.enaza.aizenberg.Preferences
import com.enaza.aizenberg.di.KoinInjector
import io.reactivex.disposables.CompositeDisposable
import moxy.InjectViewState
import moxy.MvpPresenter
import timber.log.Timber
import java.io.File
import java.io.IOException

@InjectViewState
class AudioRecordingPresenter : MvpPresenter<AudioRecordingViewContract>() {

    private var permissionGranted = false
    private var recorder: MediaRecorder? = null
    private var player: MediaPlayer? = null
    private var currentFileName: String? = null
    private var isStarted = false
    private var currentState = CurrentState.IDLE

    private val compositeDisposable = CompositeDisposable()

    private enum class CurrentState {
        IDLE,
        RECORDING,
        RECORDED,
        PLAYING,
        UPLOADING

    }

    fun setPermissionGranted() {
        permissionGranted = true
    }


    fun startRecording() {
        if (currentState == CurrentState.IDLE || currentState == CurrentState.RECORDED) {
            changeStateInternal(CurrentState.RECORDING)
            currentFileName = generateFileName()
            recorder = MediaRecorder().apply {
                setAudioSource(MediaRecorder.AudioSource.MIC)
                setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                setOutputFile(currentFileName)
                setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
                try {
                    prepare()
                    isStarted = true
                    viewState.onRecordStart()
                    start()
                } catch (e: IOException) {
                    Timber.e(e)
                    changeStateInternal(CurrentState.IDLE)
                }
            }
        }
    }

    fun startOrStopPlaying() {
        if (currentState == CurrentState.IDLE || currentState == CurrentState.RECORDING || currentState == CurrentState.UPLOADING) {
            return
        }
        if (!isFileReady()) {
            changeStateInternal(CurrentState.IDLE)
            return
        }
        if (player != null && player!!.isPlaying) {
            stopPlaying()
        } else {
            changeStateInternal(CurrentState.PLAYING)
            player = MediaPlayer().apply {
                try {
                    setDataSource(currentFileName)
                    setOnCompletionListener {
                        stopPlaying()
                    }
                    prepare()
                    start()
                } catch (e: IOException) {
                    changeStateInternal(CurrentState.IDLE)
                }
            }
        }
    }

    private fun generateFileName(isZip: Boolean = false): String {
        val extension = if (isZip) "zip" else "3gp"

        return File(
            EnazaTestApplication.getInstance().externalCacheDir,
            "${System.currentTimeMillis()}.$extension"
        ).absolutePath
    }

    fun stopRecording() {
        if (currentState == CurrentState.RECORDING) {
            changeStateInternal(CurrentState.RECORDED)
            viewState.onRecordComplete()
            recorder?.apply {
                stop()
                release()
            }
            recorder = null
            currentFileName?.let {
                if (File(it).exists() && File(it).canRead()) {
                    Timber.d("$it success write")
                }
            }
        }
    }

    private fun isFileReady(): Boolean {
        if (currentFileName == null) {
            return false
        }
        val file = File(currentFileName!!)
        return file.exists() && file.canRead() && file.length() > 0
    }

    private fun stopPlaying() {
        player?.release()
        player = null
        changeStateInternal(CurrentState.RECORDED)
    }

    fun uploadFile() {
        changeStateInternal(CurrentState.UPLOADING)
        val homeInteractor = KoinInjector.instance.homeInteractor
        compositeDisposable.add(
            homeInteractor.processFileToDocumentsAndWall(
                File(currentFileName!!),
                File(generateFileName(true)),
                Preferences.getInstance().userId.toString()
            ).subscribe({
                viewState.onUploadingInProgress(it)
            }, {
                viewState.onUploadingFailure(it.message!!)
                Timber.e(it)
            }, {
                changeStateInternal(CurrentState.IDLE)
                viewState.onUploadingEnd()
            })
        )
    }


    private fun changeStateInternal(newState: CurrentState) {
        when (newState) {
            CurrentState.IDLE -> {
                viewState.resetState()
            }
            CurrentState.RECORDING -> {
                viewState.onRecordStart()
            }
            CurrentState.RECORDED -> {
                viewState.onRecordComplete()
                viewState.activeUploadButton()
                viewState.setPlayButtonToPlayState(true)

            }
            CurrentState.PLAYING -> {
                viewState.setPlayButtonToStopState()

            }
            CurrentState.UPLOADING -> {
                viewState.setPlayButtonToPlayState(false)
                viewState.deactivateUploadButton()
                viewState.onUploadingStarted()
            }
        }
        currentState = newState
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
        super.onDestroy()

    }


}