package com.enaza.aizenberg.ui.home.child.record

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndStrategy::class)
interface AudioRecordingViewContract : MvpView {

    fun onRecordStart()

    fun onRecordComplete()

    fun onRecordFailure(cause: String)

    fun setPlayButtonToPlayState(isEnabled: Boolean)

    fun setPlayButtonToStopState()

    fun activeUploadButton()

    fun deactivateUploadButton()

    fun resetState() {
        deactivateUploadButton()
        setPlayButtonToPlayState(false)
    }

    fun onUploadingStarted()

    fun onUploadingInProgress(progress: Double)

    fun onUploadingEnd()

    fun onUploadingFailure(cause: String)

}