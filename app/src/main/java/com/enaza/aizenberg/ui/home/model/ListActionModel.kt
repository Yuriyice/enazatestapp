package com.enaza.aizenberg.ui.home.model

import androidx.annotation.DrawableRes

data class ListActionModel(
    @DrawableRes val actionDrawResId: Int,
    val actionName: String,
    val actionDescription: String,
    val actionType: ActionType
) {

    enum class ActionType {
        AUDIO_RECORD,
        PHOTO
    }


}
