package com.enaza.aizenberg.ui.start.login

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebView
import android.webkit.WebViewClient
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.login.VKLoginResponse
import com.enaza.aizenberg.ui.start.welcome.WelcomeActivity
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter

class LoginActivity : MvpAppCompatActivity(), LoginContract {


    lateinit var webViewLogin: WebView
    private val presenter by moxyPresenter { LoginPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        webViewLogin = findViewById(R.id.webViewLogin)


        val webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                url?.let {
                    if (url.startsWith("https://oauth.vk.com/blank.html")) {
                       presenter.parseResult(url)
                    }
                }
            }
        }

        webViewLogin.webViewClient = webViewClient

        webViewLogin.loadUrl(buildUrl())

    }

    override fun onLoginSuccess(result: VKLoginResponse.IVKLoginResponse) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(WelcomeActivity.AUTHORIZATION_RESULT_KEY, result)
        })
        finish()
    }

    override fun onLoginFailure(result: VKLoginResponse.IVKLoginResponse) {
        //Skip this callback because error should be showing inside webview
    }


    private fun buildUrl(): String {
        return Uri.Builder()
            .scheme("https")
            .authority("oauth.vk.com")
            .appendPath("authorize")
            .appendQueryParameter(
                "client_id",
                resources.getInteger(R.integer.com_vk_sdk_AppId).toString()
            )
            .appendQueryParameter("redirect_uri", "https://oauth.vk.com/blank.html")
            .appendQueryParameter("display", "mobile")
            .appendQueryParameter("scope", "139268")
            .appendQueryParameter("response_type", "token")
            .appendQueryParameter("v", "5.103")
            .appendQueryParameter("state", STATE)
            .build()
            .toString()
    }


    companion object {
        private const val STATE = "1424112"

    }

}