package com.enaza.aizenberg.ui.start.login

import com.enaza.aizenberg.domain.entity.login.VKLoginResponse
import moxy.MvpView
import moxy.viewstate.strategy.SingleStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = SingleStateStrategy::class)
interface LoginContract : MvpView {

    fun onLoginSuccess(result: VKLoginResponse.IVKLoginResponse)

    fun onLoginFailure(result: VKLoginResponse.IVKLoginResponse)


}