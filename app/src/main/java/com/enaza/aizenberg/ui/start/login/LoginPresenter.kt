package com.enaza.aizenberg.ui.start.login

import android.net.Uri
import com.enaza.aizenberg.domain.entity.login.VKLoginResponse
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class LoginPresenter : MvpPresenter<LoginContract>() {


    fun parseResult(result: String) {
        val parse = Uri.parse(result.replace("#", "?"))
        val errorParameter = parse.getQueryParameter("error")
        if (errorParameter != null) {
            viewState.onLoginFailure(
                VKLoginResponse.failure(
                    errorParameter,
                    parse.getQueryParameter("error_description") ?: ""
                )
            )
        } else {
            val accessToken = parse.getQueryParameter("access_token")!!
            val expiresIn = parse.getQueryParameter("expires_in")!!.toLong()
            val userId = parse.getQueryParameter("user_id")!!.toLong()
            viewState.onLoginSuccess(VKLoginResponse.success(expiresIn, accessToken, userId))
        }
    }


}