package com.enaza.aizenberg.ui.start.welcome

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.login.VKLoginResponse
import com.enaza.aizenberg.ui.home.HomeActivity
import com.enaza.aizenberg.ui.start.login.LoginActivity
import com.enaza.aizenberg.utils.disable
import com.enaza.aizenberg.utils.enable
import com.enaza.aizenberg.utils.showErrorSnack
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter


class WelcomeActivity : MvpAppCompatActivity(), WelcomeContract {

    private val presenter by moxyPresenter { WelcomePresenter() }

    lateinit var btnStartAuthorization: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        btnStartAuthorization = findViewById<View>(R.id.btnStartAuthorization)
        presenter.checkAuthorization()
        btnStartAuthorization.setOnClickListener { presenter.startAuthorization() }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTHORIZATION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val resultObject = data?.getSerializableExtra(AUTHORIZATION_RESULT_KEY) as VKLoginResponse.IVKLoginResponse
            presenter.onAuthorizationResult(resultObject)
        }
    }

    override fun startLoginFlow() {
        startActivityForResult(Intent(this, LoginActivity::class.java), AUTHORIZATION_REQUEST_CODE)
    }

    override fun startHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override fun disableLoginButton() {
        btnStartAuthorization.disable()
    }

    override fun enableLoginButton() {
        btnStartAuthorization.enable()
    }

    override fun onError(cause: String) {
        showErrorSnack(cause)
    }

    companion object {
        const val AUTHORIZATION_REQUEST_CODE = 15555
        const val AUTHORIZATION_RESULT_KEY = "ark"


    }
}
