package com.enaza.aizenberg.ui.start.welcome

import com.enaza.aizenberg.ui.BaseMvpView
import moxy.MvpView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType


@StateStrategyType(value = OneExecutionStateStrategy::class)
interface WelcomeContract : BaseMvpView {

    fun startLoginFlow()

    fun startHomeScreen()

    fun disableLoginButton()

    fun enableLoginButton()

}