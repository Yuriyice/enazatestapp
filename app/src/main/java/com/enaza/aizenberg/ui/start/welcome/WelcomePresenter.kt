package com.enaza.aizenberg.ui.start.welcome

import com.enaza.aizenberg.Preferences
import com.enaza.aizenberg.domain.entity.base.VKRequestError
import com.enaza.aizenberg.domain.entity.login.VKLoginResponse
import com.enaza.aizenberg.domain.exception.VKApiRequestException
import com.enaza.aizenberg.ui.start.welcome.WelcomeContract
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.concurrent.TimeUnit


@InjectViewState
class WelcomePresenter : MvpPresenter<WelcomeContract>() {

    fun checkAuthorization() {
        viewState.disableLoginButton()
        if (Preferences.getInstance().isAuthorized()) {
            viewState.startHomeScreen()
        } else {
            viewState.enableLoginButton()
        }
    }

    fun startAuthorization() {
        viewState.disableLoginButton()
        viewState.startLoginFlow()
    }

    fun onAuthorizationResult(response: VKLoginResponse.IVKLoginResponse) {
        viewState.enableLoginButton()
        if (response.isSuccess()) {
            val success = response as VKLoginResponse.Success
            with(Preferences.getInstance()) {
                expireIn = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(success.expiresIn)
                userId = success.userId
                token = success.accessToken
            }
            viewState.startHomeScreen()
        } else {
            viewState.onError((response as VKLoginResponse.Failure).errorDescription)
        }
    }

}