package com.enaza.aizenberg.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.enaza.aizenberg.R
import com.enaza.aizenberg.domain.entity.profile.VKProfileResponse

class CurrentUserView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val userImageView: ImageView
    private val txtUserName : TextView
    var onLogout : (() -> Unit) ?= null

    init {
        View.inflate(context, R.layout.view_current_user_panel, this)
        userImageView = findViewById(R.id.imgUserAvatar)
        txtUserName = findViewById(R.id.txtUserName)
        findViewById<View>(R.id.llLogout).setOnClickListener { onLogout?.invoke() }
    }

    @SuppressLint("SetTextI18n")
    fun setProfile(vkProfile: VKProfileResponse) {
        Glide.with(context).load(vkProfile.photo).centerCrop().placeholder(R.drawable.bg_user_placeholder).into(userImageView)
        txtUserName.text = "${vkProfile.firstName} ${vkProfile.lastName}"
    }



}