package com.enaza.aizenberg.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout

class RecordTouchHolderOverlay @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var touchListener: IOverlayListener? = null
    private var isTouched = false

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) return super.onTouchEvent(event)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchListener?.onStartTouch()
            MotionEvent.ACTION_UP -> touchListener?.onEndTouch()
        }
        return true
    }


    interface IOverlayListener {

        fun onStartTouch()

        fun onEndTouch()

    }
}