package com.enaza.aizenberg.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.enaza.aizenberg.R
import timber.log.Timber
import kotlin.math.roundToInt

class UploadingProgressView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private val animationView: LottieAnimationView
    private val txtUploadingPercentage: TextView


    init {
        inflate(context, R.layout.view_uploading, this)
        animationView = findViewById(R.id.viewAnimUploading)
        txtUploadingPercentage = findViewById(R.id.txtUploadingPercentage)
    }

    fun reset() {
        animationView.pauseAnimation()
        txtUploadingPercentage.text = "0%"
    }

    fun start() {
        animationView.playAnimation()
    }

    @SuppressLint("SetTextI18n")
    fun updateProgress(value: Double) {
        val percentage = (value * 100).roundToInt()
        Timber.d("Uploading UI $percentage")
        txtUploadingPercentage.text = "$percentage%"
    }

    fun stop() {
        animationView.pauseAnimation()
    }
}