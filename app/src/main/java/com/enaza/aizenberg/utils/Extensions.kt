package com.enaza.aizenberg.utils

import java.io.Closeable


fun Closeable?.closeSafe() {
    if (this == null) return
    try {
        close()
    } catch (e: Exception) {
        //Ignore
    }
}