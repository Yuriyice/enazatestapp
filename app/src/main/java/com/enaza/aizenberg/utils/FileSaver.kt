package com.enaza.aizenberg.utils

import android.graphics.Bitmap
import io.reactivex.Single
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class FileSaver(private val bitmap: Bitmap, private val targetFile: File) {

    fun save(): Single<File> {
        return Single.create { emitter ->
            try {
                if (!targetFile.exists()) {
                    val newFile = targetFile.createNewFile()
                    if (!newFile) {
                        throw IOException("Unable to create file")
                    }
                }
                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
                val bitmapdata = bos.toByteArray()

                //write the bytes in file
                FileOutputStream(targetFile).use {
                    it.write(bitmapdata)
                    it.flush()
                }
                emitter.onSuccess(targetFile)
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

}