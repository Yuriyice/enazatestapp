package com.enaza.aizenberg.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import io.nlopez.smartlocation.SmartLocation
import io.nlopez.smartlocation.location.providers.LocationManagerProvider

object GeoService {

    var location: Location? = null

    get() {
        return field ?: smartLocation?.location()?.lastLocation
    }

    @SuppressLint("StaticFieldLeak")
    private var smartLocation: SmartLocation? = null

    fun startGeoListening(context: Context) {
        stopGeoListening()
        smartLocation = SmartLocation.with(context).apply {
            location(LocationManagerProvider()).oneFix().start {
                location = it
            }
        }

    }

    fun stopGeoListening() {
        smartLocation?.location()?.stop()
    }

}