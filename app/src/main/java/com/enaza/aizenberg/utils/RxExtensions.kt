package com.enaza.aizenberg.utils

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yuriy Aizenberg
 */
fun <T> Flowable<T>.withMultithreadProcessor(): Flowable<T> {
    return compose(RxUtils.applyFlowable())
}

fun <T> Flowable<T>.withSinglethreadProcessor(): Flowable<T> {
    return compose(RxUtils.applySingleThreadFlowable())
}

fun <T> Flowable<T>.withSinglethreadProcessorAlt(): Flowable<T> {
    return compose(RxUtils.applySingleThreadFlowableAlternative())
}

fun <T> Flowable<T>.withSinglethreadProcessorGrabber(): Flowable<T> {
    return compose(RxUtils.applySingleThreadFlowableGrabber())
}

fun <T> Single<T>.withMultithreadProcessor(): Single<T> {
    return compose(RxUtils.applySingle())
}

fun <T> Maybe<T>.withMultithreadProcessor(): Maybe<T> {
    return compose(RxUtils.applyMaybe())
}

fun Completable.withMultithreadProcessor(): Completable {
    return compose(RxUtils.applyCompletable())
}

fun <T> Flowable<T>.applyDefaultScheduler(): Flowable<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

