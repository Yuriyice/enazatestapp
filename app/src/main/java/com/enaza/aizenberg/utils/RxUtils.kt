package com.enaza.aizenberg.utils

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

/**
 * Created by Yuriy Aizenberg
 */
object RxUtils {

    private val singleThreadExecutor = Executors.newSingleThreadExecutor()
    private val singleThreadExecutorAlt = Executors.newSingleThreadExecutor()
    private val singleThreadExecutorGrabber = Executors.newSingleThreadExecutor()

    fun <T> applyFlowable(): FlowableTransformer<T, T> {
        return FlowableTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applyObservable(): ObservableTransformer<T, T> {
        return ObservableTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applyObvervableSingle(): ObservableTransformer<T, T> {
        return ObservableTransformer { it.subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applyMaybe(): MaybeTransformer<T, T> {
        return MaybeTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applySingle(): SingleTransformer<T, T> {
        return SingleTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun applyCompletable(): CompletableTransformer {
        return CompletableTransformer { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applySingleThreadFlowable(): FlowableTransformer<T, T> {
        return FlowableTransformer { it.subscribeOn(Schedulers.from(singleThreadExecutor)).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applySingleThreadFlowableAlternative(): FlowableTransformer<T, T> {
        return FlowableTransformer { it.subscribeOn(Schedulers.from(singleThreadExecutorAlt)).observeOn(AndroidSchedulers.mainThread()) }
    }

    fun <T> applySingleThreadFlowableGrabber(): FlowableTransformer<T, T> {
        return FlowableTransformer { it.subscribeOn(Schedulers.from(singleThreadExecutorGrabber)).observeOn(AndroidSchedulers.mainThread()) }
    }
}




