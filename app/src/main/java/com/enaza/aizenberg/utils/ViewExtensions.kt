package com.enaza.aizenberg.utils

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

fun View.disable() {
    isEnabled = false
}

fun View.enable() {
    isEnabled = true
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}


fun View.gone() {
    visibility = View.GONE
}

fun AppCompatActivity.showErrorSnack(message: String) {
    Snackbar.make(window.decorView.rootView, message, Snackbar.LENGTH_LONG).show()
}

