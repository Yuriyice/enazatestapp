package com.enaza.aizenberg.utils

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class ZipManager {

    fun zip(filePath: String, zipFileName: String) {
        var origin: BufferedInputStream? = null
        var out: ZipOutputStream? = null
        try {

            val dest = FileOutputStream(zipFileName)
            out = ZipOutputStream(
                BufferedOutputStream(
                    dest
                )
            )
            val data = ByteArray(BUFFER)
            val fi = FileInputStream(filePath)
            origin = BufferedInputStream(fi, BUFFER)

            val entry = ZipEntry(filePath.substring(filePath.lastIndexOf("/") + 1))
            out.putNextEntry(entry)
            var count: Int

            count = origin.read(data, 0, BUFFER)
            do {
                out.write(data, 0, count)
                count = origin.read(data, 0, BUFFER)
            } while (count != -1)
        } finally {
            origin.closeSafe()
            out.closeSafe()
        }

    }

    companion object {
        private const val BUFFER = 80000
    }

}